
function assigned_dataset = assign_markers(dataset,mass_tol,reten_tol,mode,method)

% I/O: assigned_dataset = assign_markers(dataset,mass_tol,reten_tol,mode,method)
% 
% INPUT:    
%           dataset:    Is a Dataset Object containing the data and labels
%                       named 'retention time' and 'mass'.
% 
%           mass_tol:   The mass tolerance in unit Dalton (the function
%                       will search for masses +/- the tolerance).
% 
%           reten_tol:  The retention time tolerance in minutes (the function
%                       will search for retention times +/- the tolerance).
% 
%           mode:       Ionization mode. Can be 'neg' or 'pos'.
% 
%           method:     The chromatographic method. Can be 'old' or 'new'.
% 
% OUTPUT:   
%           assigned_dataset: New dataset with compounds assigned as a
%                             label.
% 


% Load the database with all the known compounds
% Getting the right masses and retention times
load compound_database;


select = strcmpi(compound_database.mode,mode) &  strcmpi(compound_database.method,method)    &    ~isnan(compound_database.mz)   &    ~isnan(compound_database.rt);


mass       = compound_database.mz(select);
reten      = compound_database.rt(select);

temp1  = compound_database.ID(select);
temp2  =compound_database.anno(select);
comp_name = str2cell(strcat(cell2str(temp1),' (',cell2str(temp2),')'));
clear temp1 temp2




% I need to know where to write the marker names when I find them
% I need to make an empty label to put the names into
free_label_pos = size(dataset.label(2,:),2)+1;
dataset.label{2,free_label_pos} = repmat(' ',size(dataset,2),1);
dataset.labelname{2,free_label_pos} = 'ID';


% I need to know where the retention time and mass data is in the dataset.
if ~any(any(ismember(lower(dataset.labelname),'mass')))
    disp('I cannot find the masses! Please read the help.')
end
if ~any(any(ismember(lower(dataset.labelname),'retention time')))
    disp('I cannot find the retention times! Please read the help.')
end

    for mass_loc = 1:size(dataset.labelname,2)
        if strcmpi(  dataset.labelname{2,mass_loc}  ,  'mass')
            break
        end
    end
    for tr_loc = 1:size(dataset.labelname,2)
        if strcmpi(  dataset.labelname{2,tr_loc}  ,  'retention time')
            break
        end
    end

% Find the marker and putting in the name in the dataset
for i = 1:size(comp_name,1)
    % Finding where the compounds are
    hit = ...
      str2num(dataset.label{2,mass_loc}) > (mass(i) - mass_tol)...
    & str2num(dataset.label{2,mass_loc}) < (mass(i) + mass_tol)...
    & str2num(dataset.label{2,tr_loc}) > (reten(i) - reten_tol)...
    & str2num(dataset.label{2,tr_loc}) < (reten(i) + reten_tol);


    % Did we find it and is it unique?
    result = sum(hit);
    result_summary(i) = result;

    
    % Make string. Add "non-unique" if we cannot get a unique hit for the
    % compound.
    if result > 1 % More than one hit. We don't know which is right
        ID = cell2str(strcat(comp_name(i),' (non-unique hit)'));
    end

    if result == 1 % YES! we know where the marker is
        ID = cell2str(comp_name(i));
    end
    

     if result ~= 0
      

        % Where should we place the marker?
        hit_loc = find(hit);

        for i_2 = 1:length(hit_loc)
            current_hit_loc = hit_loc(i_2);
            if isempty(strtrim(dataset.label{2,free_label_pos}(current_hit_loc,:)))
                dataset.label{2,free_label_pos}{current_hit_loc} = strtrim(ID);
                mul_assign(i) = 0;
                
            else
                dataset.label{2,free_label_pos}{current_hit_loc} = horzcat(strtrim(dataset.label{2,free_label_pos}(current_hit_loc,:)),' OR ',strtrim(ID));
                mul_assign(i) = 1;
                
            end
        end
    
    
     end
    
    
    clear hit ID
end



assigned_dataset = dataset;

disp('SUMMARY:')
disp(strcat(num2str(sum(result_summary == 1)),' Unique hits'))
disp(strcat(num2str(sum(result_summary > 1)),' Non-unique hits'))
disp(strcat(num2str(sum(result_summary == 0)),' Compounds not found'))
if exist('mul_assign')
disp(strcat(num2str(sum(mul_assign)),' Markers had multiple compounds assigned'))
end
end

