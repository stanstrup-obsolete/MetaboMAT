function dataset_var = make_dataset(marker_data,sample_ID,marker_info,marker_info_desc,sample_info,sample_desc_title)

% This function takes several variables and combines them in a dataset file
% for use with PLS toolbox.
%
% The following data structure is expected:
% ___________________________________________________
% |   |    sample_desc_title    |    marker_info
% |___|_________________________|____________________
% |s  |                         |
% |a  |                         |
% |m  |                         |
% |p  |                         |
% |l  |    sample_info          |    marker_data
% |e  |                         |
% |   |                         |
% |I  |                         |
% |D  |                         |
% |   |                         |
%
%
% marker_data is a numeric matrix with the raw data.
% marker_info is a matrix containing marker numbers, retention time and/or mass.
%
% sample_ID is a vector with the names of each variable.
%
% sample_info is a numeric matrix with class information about the samples.
% sample_desc_title is a vector with the names of each class column in
% sample_info.





    % Making with with raw data only
    dataset_var               = dataset(marker_data);


    % Adding sample names
    dataset_var.label{1}      = sample_ID;


    % Adding marker name/numbers
    for i_2 = 1:size(marker_info,1)
        for i=1:size(marker_info,2)
            a{i} = num2str(cell2mat(marker_info(i_2,i)));
        end
        dataset_var.label{2,i_2}    = a;
    end


    % Giving the marker_info names
    for i=1:size(marker_info_desc,2)
        dataset_var.labelname{2,i}=marker_info_desc(:,i);
    end
    
    
    % Making classes of the sample info
    for i=1:size(sample_info,2)
        dataset_var.class{1,i}=sample_info(:,i);
    end


    % Giving the classes names
    for i=1:size(sample_desc_title,2)
        dataset_var.classname{1,i}=sample_desc_title(:,i);
    end

end

