function y = findlabel( dataset,dim,labelname )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

% I need to know where the retention time are in the dataset.
if ~any(any(ismember(lower(dataset.labelname(dim,:)),lower(labelname))))
    disp('I cannot find the label! Please read the help.')
end

for y = 1:size(dataset.labelname(dim,:),2)
    if strcmpi(  dataset.labelname{dim,y}  ,  labelname)
        break
    end
end



end





