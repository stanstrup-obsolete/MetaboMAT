function [study_samples,std_samples] = split_samples(dataset_input,defining_class)
%
% [study_samples,std_samples] = split_samples(dataset_input,defining_class)
%
% This function splits a dataset file into seperate datasets for study
% samples and standard samples based on class asigments.
%
% study_samples:    Variable name for the dataset with only study samples
% std_samples:      Variable name for the dataset with only standard samples
%
% dataset_input:    Input dataset file
% defining_class:   Name of the class to use for seperation into study
%                   samples and standard samples; E.g. 'Meal', 'Time' etc.
%
% It is asumed that the class has 0 for standard samples.


%% Making selection vector
temp_marker_data            = dataset_input.data;
temp_sample_info            = dataset_input.class;
temp_sample_desc_title      = dataset_input.classname;



% Finding the column number for the chosen classname
for i = 1:length(temp_sample_desc_title)
    if strcmp(  temp_sample_desc_title{1,i}  ,  defining_class)
        break
    end
end

% Making logical vector showing which rows belong
selection_vector = temp_sample_info{1,i} ~= 0;
clear i



study_samples = delsamps(dataset_input,find(~selection_vector),1,2);
std_samples = delsamps(dataset_input,find(selection_vector),1,2);






%     %% Creating the study sample set
%
%     % Creating the file with just data
%     study_samples = dataset(temp_marker_data(selection_vector,:));
%
%
%     % Adding sample names
%     temp=dataset_input.label{1};
%     temp=str2cell(temp);
%     temp = temp(selection_vector);
%     study_samples.label{1}     = temp;
%     clear temp
%
%
%     % Adding class data
%     for i = 1:length(dataset_input.classname)
%            temp = temp_sample_info;
%            temp{1,i}(~selection_vector) = [];
%            temp = temp{1,i};
%            study_samples.class{1,i} = temp;
%     end
%     clear temp
%
%
% 	% Adding class names
%     for i=1:size(dataset_input.classname,2)
%         study_samples.classname{1,i}=dataset_input.classname(1,i);
%     end
%
%
%     % Adding marker name/numbers
%     for i = 1:size(  dataset_input.label ,2  )
%     study_samples.label{2,i}   =  dataset_input.label{2,i};
%     end
%
%     % Giving the marker_info names
%     for i = 1:size(  dataset_input.label ,2  )
%     study_samples.labelname{2,i}   =  dataset_input.labelname{2,i};
%     end
%
%
%     %% Creating the standard sample set
%
%     % Creating the file with just data
%     std_samples = dataset(temp_marker_data(~selection_vector,:));
%
%
%     % Adding sample names
%     temp=dataset_input.label{1};
%     temp=str2cell(temp);
%     temp = temp(~selection_vector);
%     std_samples.label{1}     = temp;
%     clear temp
%
%
%     % Adding class data
%     for i = 1:length(dataset_input.classname)
%            temp = temp_sample_info;
%            temp{1,i}(selection_vector) = [];
%            temp = temp{1,i};
%            std_samples.class{1,i} = temp;
%     end
%     clear temp
%
%
% 	% Adding class names
%     for i=1:size(dataset_input.classname,2)
%         std_samples.classname{1,i}=dataset_input.classname(1,i);
%     end
%
%
%     % Adding marker name/numbers
%     for i = 1:size(  dataset_input.label ,2  )
%     std_samples.label{2,i}   =  dataset_input.label{2,i};
%     end
%
%     % Giving the marker_info names
%     for i = 1:size(  dataset_input.label ,2  )
%     std_samples.labelname{2,i}   =  dataset_input.labelname{2,i};
%     end
end