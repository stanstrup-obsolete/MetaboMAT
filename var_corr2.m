function correct_set = var_corr2(dataset_input,corr_var)
% 
% This function normalises at sample group level.
%
% Each value in each group is multiplied by the mean of all other groups.
% For the resulting data the mean of each variables is the same for all
% sample groups.
% For example all values in group1 is multiplied by:
% mean(group2)*mean(group3)......mean(groupN)
%
% All zeros are set to NaN.
%
% INPUT:
% dataset_input:    Dataset file to be corrected.
% corr_var:         The name (as defined in .classname) of the class to be
%                   used for grouping.
% 
% OUTPUT: 
% correct_set:      Corrected dataset  file.
% 
% I/O: correct_set = var_corr1(dataset_input,corr_var)

% Extracting the data from the dataset
data                   = dataset_input.data;
sample_desc_title      = dataset_input.classname;
sample_info            = dataset_input.class;

% Other vars that will be calculated
%
% i:        The number of the column in sample_info that holds the data for
%           the class specified in corr_var. This is used for getting data
%           for only one group of data.
% 
% group:    Vector with only the grouping class data.
% 
% means:    Matrix where each column corresponds to a varible of the data
%           and each row is the mean of each group.


% setting zeros to NaN
data(data==0)=NaN;

% Finding the column number for the correction var
if ~any(any(   ismember(sample_desc_title,corr_var)   ))
    error('Class name not found. Please re-check parameters. The class name need to be defined in dataset_input.classname')
end    

for i = 1:length(sample_desc_title)
	if strcmp(  sample_desc_title{1,i}  ,  corr_var)
        break
	end
end   
    

% Calculation means for each variable for each group
group = sample_info{1,i};
means = zeros(  length(unique(group)),  size(data,2)   );

for i_2 = 1:length(   unique(group)   )
    group_num=unique(group);
    group_num=group_num(i_2);
    
   means(i_2,:)        =       nanmean(   data(group==group_num,:)   );    
end
clear i_2

    
% Calculating the corrected data
for i_2 = 1:length(   unique(group)   )
    group_num=unique(group);
    group_num=group_num(i_2);
    
    for i_3 = 1:size(data,2)
    single_group_single_var   =   data(group==group_num,i_3) .*  ( prod(means(:,i_3)) / means(i_2,i_3)  );
    
        if ~exist('single_group_data','var')
            single_group_data = single_group_single_var ;
        else
            single_group_data = [single_group_data single_group_single_var] ;
        end
    end
    
    
    
    if ~exist('new_data','var')
        new_data = single_group_data ;
        clear single_group_data
	else
        new_data = [new_data;single_group_data];
        clear single_group_data
    end
    
end
clear i_2 i_3
    
% new_data(isnan(new_data)) = 0;   
correct_set = dataset_input;
correct_set.data = new_data;
end