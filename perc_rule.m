function reduced_dataset = perc_rule(dataset_input,setpoint,group_class1,group_class2)

%% reduced_marker_dataset = perc_rule(dataset_input,setpoint,group_class1,group_class2)
%
% Takes a dataset file and removes variables not present on a
% specified fraction of the samples in at least one of the specified sample
% groups.
% This function asumes that two groups, e.g. meal type and sampling time,
% are merged into new combined groups.
% To use it for only one group (e.g. meal type) set parameters for group
% one and two to the same value.
%
% See:  Bijlsma, S., Bobeldijk, I., Verheij, E. R., Ramaker, R. et al.,
%       Anal. Chem. 2006, 78, 567�574.
% 
%
% INPUT:    dataset_input    Dataset file including classes, class names 
%                            and sample names.
%           setpoint         Required fraction of non-zero samples in each 
%                            group; e.g. 0.8 for a "80% rule".
%           group_class1     The name (as defined in .classname) of the
%                            first class to be used for grouping.
%           group_class2     The name (as defined in .classname) of the
%                            second class to be used for grouping.
%
%
% OUTPUT:   reduced_marker_dataset  New dataset with only selected variables.
%                              
%
% EXAMPLE:  study_samples_80_rule = perc_rule(study_samples,0.8,'Meal','Time')

%% Making the vector for variables selection
marker_data            =    dataset_input.data;
sample_info            =    dataset_input.class;
sample_desc_title      =    dataset_input.classname;


% Checking that class names exist
if ~any(any(   ismember(sample_desc_title,group_class1)   )) || ~any(any(   ismember(sample_desc_title,group_class2)   ))
    error('Class name not found. Please re-check parameters. The class name need to be defined in dataset_input.classname')
end 

% Finding the column number for the first group
for group1 = 1:length(sample_desc_title)
	if strcmp(  sample_desc_title{1,group1}  ,  group_class1)
        break
	end
end   

% Finding the column number for the second group
for group2 = 1:length(sample_desc_title)
	if strcmp(  sample_desc_title{1,group2}  ,  group_class2)
        break
	end
end  


% Finding the range of group values 
group1ID = unique(  sample_info{1,group1}  );
group2ID = unique(  sample_info{1,group2}  );




% Making logical zero matrix with same length as there are columns in
% marker_data. 0 and 1 will be put here to select with variables to keep.
non_zero_samples_comb = false(1, size( marker_data, 2));


% Creating groups by combining the two defined groups. Then doing the
% following commands in sequence on these new groups.
for cm = group1ID
    for ct = group2ID
        % The data for each new grouping is put into a new variable,
        % single_group_data; one group at a time.
        single_group_data = marker_data( sample_info{ 1, group1} == cm & sample_info{ 1, group2} == ct, :);
        
        % The number of non-zero samples (rows) in each row is devided by the
        % total number samples (rows). This is done idividualle for each
        % variable (column).
        non_zero_samples = sum( single_group_data > 0)/ size( single_group_data, 1);
        
        % non_zero_samples is changed into a logical vector defining which
        % variables (columns) meet the setpoint requirement.
        non_zero_samples = non_zero_samples > setpoint;
        
        % Making variable, non_zero_samples_comb, that has logical 1 where
        % non_zero_samples is 1 in any of the new groups (i.e. in any of the loops).
        non_zero_samples_comb(non_zero_samples) = true;
    end
end


%% Making new dataset


reduced_dataset = delsamps(dataset_input,find(~non_zero_samples_comb),2,2);