MetaboMAT
=========

A collection of matlab functions useful for dealing with LC-MS metabolomics data. Most are meant to be used together with PLS toolbox.



* **assign_markers**: Annotate a PLS toolbox dataset based on a compound database (rt / m/z).
* **group_markers**: Groups features in a PLS toolbox dataset based on correlations across samples. Similar to the R package CAMERA.
* **make_dataset**: Convenience function to create a PLS toolbox dataset containing: raw data, sample names, sample class, retention time and mass.
* **perc_rule**: Takes a dataset file and removes variables not present on a specified fraction of the samples in at least one of the specified sample groups.
* **split_samples**: This function splits a dataset file into seperate datasets for study % samples and standard samples based on class asigments.
* **var_corr2**: This function normalises at sample group level (AKA batch correction). For the resulting data the mean of each variable is the same for all sample groups.
* **var_corr3**: Same as above but if a group only contains NaN the mean is set to 1.




