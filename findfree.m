function free_field_pos = findfree( dataset, field, direction)

% I/O: free_field_pos = findfree( datasetset, field, direction)
%
%
%
%
%

switch field
    
    case 'label'
        free_field_pos = find(strcmp(dataset.label(direction,:),'')==1,1);
        
        if cellfun('isempty',dataset.label(direction,:))
            free_field_pos  =   find(cellfun('isempty',dataset.label(direction,:)));
        end
        
        if isempty(free_field_pos)
            free_field_pos=size(dataset.label(direction,:),2)+1;
        end
        
        
        
    case 'labelname'
        free_field_pos = find(strcmp(dataset.labelname(direction,:),'')==1,1);
        
        if cellfun('isempty',dataset.labelname(direction,:))
            free_field_pos  =   find(cellfun('isempty',dataset.labelname(direction,:)));
        end
        
        if isempty(free_field_pos)
            free_field_pos=size(dataset.labelname(direction,:),2)+1;
        end
        
        %-------------------------------------------------------
        
    case 'class'
        free_field_pos = find(strcmp(dataset.class(direction,:),'')==1,1);
        
        if cellfun('isempty',dataset.class(direction,:))
            free_field_pos  =   find(cellfun('isempty',dataset.class(direction,:)));
        end
        
        if isempty(free_field_pos)
            free_field_pos=size(dataset.class(direction,:),2)+1;
        end
        
    case 'classname'
        free_field_pos = find(strcmp(dataset.classname(direction,:),'')==1,1);
        
        if cellfun('isempty',dataset.classname(direction,:))
            free_field_pos  =   find(cellfun('isempty',dataset.classname(direction,:)));
        end
        
        if isempty(free_field_pos)
            free_field_pos=size(dataset.classname(direction,:),2)+1;
        end
        
        
        %-------------------------------------------------------
        
    case 'axisscale'
        free_field_pos = find(strcmp(dataset.axisscale(direction,:),'')==1,1);
        
        if cellfun('isempty',dataset.axisscale(direction,:))
            free_field_pos  =   find(cellfun('isempty',dataset.axisscale(direction,:)));
        end
        
        if isempty(free_field_pos)
            free_field_pos=size(dataset.axisscale(direction,:),2)+1;
        end
        
    case 'axisscalename'
        free_field_pos = find(strcmp(dataset.axisscalename(direction,:),'')==1,1);
        
        if cellfun('isempty',dataset.axisscalename(direction,:))
            free_field_pos  =   find(cellfun('isempty',dataset.axisscalename(direction,:)));
        end
        
        if isempty(free_field_pos)
            free_field_pos=size(dataset.axisscalename(direction,:),2)+1;
        end
end


free_field_pos = min(free_field_pos);



end

